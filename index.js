"use strict";

const express = require("express");
const https = require("https");
const socketio = require("socket.io");
const bodyParser = require("body-parser");
const fs = require("fs");
const path = require("path");
const cron = require('node-cron');

const socketEvents = require("./utils/socket");
const routes = require("./utils/routes");
const config = require("./utils/config");
const { encryptMiddleWare } = require("./utils/encrypt");
const convertLottie = require("./convert_lottie")

const packageDetails = require("./package.json");

require("dotenv").config({ path: __dirname + "/.env" });

console.log("Platform: ", process.platform);
console.log("Node version: ", process.version);
console.log("Node dependencies: ", process.versions);
console.log("Server version: ", packageDetails.version);
console.log("Database: ", process.env.DATABASE);
let keys = Object.keys(packageDetails.dependencies);

var ports = [process.env.SOCKET_PORT];
var servers = [];

class Server {
  constructor() {
    this.host = "0.0.0.0";
    this.app = express();
    this.app.use(express.static("public"));
    cron.schedule('*/5 * * * *', () => {
      convertLottie(function(error) {
        if (error) {
            console.log('-------------------------------------------------------------');
            console.log('ERROR: ',error);
            console.log('-------------------------------------------------------------');
        } else {
            // console.log('-------------------------------------------------------------');
            // console.log('Finished.');
            // console.log('-------------------------------------------------------------');
        }
      })
    });
    console.log("Modules: ");
    keys.forEach((k) => {
      console.log(`${k}: ${packageDetails.dependencies[k]}`);
    });
  }

  appConfig() {
    this.app.use(bodyParser.json());
    new config(this.app);
  }

  /* Including app Routes starts*/
  async includeRoutes(app) {
    var settingObject = {};
    var giftsObject = {};

    try {
      var data = fs.readFileSync(
        path.join(__dirname, "ssl_cert_path.text"),
        "utf8"
      );

      var certPath = data.toString();
      console.log("Cert path: ", certPath);

      var serverOptions = {
        key: fs.readFileSync(certPath + "/privkey.pem"),
        cert: fs.readFileSync(certPath + "/fullchain.pem"),
      };

      ports.forEach(async function (port) {
        var httpsServer = https.createServer(serverOptions, app);
        httpsServer.listen(port, () => {
          console.log(`Server listening on port ${port}`);
        });
        servers.push(httpsServer);

        var io = socketio(httpsServer);
        io.use(encryptMiddleWare(process.env.SOCKET_SECRET));
        await new routes(app, io).routesConfig(settingObject);
        new socketEvents(io).socketConfig(settingObject, giftsObject);
      });
    } catch (e) {
      console.log("Error:", e.stack);
    }
  }

  /* Including app Routes ends*/

  arrayToObject(array, keyField) {
    var returnObj = array.reduce((obj, item) => {
      obj[item[keyField]] = item;
      return obj;
    }, {});
    return returnObj;
  }

  appExecute() {
    this.appConfig();
    this.includeRoutes(this.app);
  }
}

const app = new Server();
app.appExecute();

//------------------------------------------------------------------------------
// HTTP Server

var expressApp = express();

//API for verifying server is running, show latest git commit info
expressApp.get("/", function (req, res) {
  var cmd = "git log -n 1";
  var exec = require("child_process").exec;
  exec(cmd, function (error, stdout, stderr) {
    if (error !== null) {
      var msg = "Error during the execution of git command: " + stderr;
      return res.send(msg);
    }
    res.status(200).send("Current git commit: " + stdout);
  });
});

//------------------------------------------------------------------------------
// HTTP Server

//Hook for gitlab auto deploy
expressApp.post("/gitlab", function (req, res) {
  console.log("Received a gitlab hook event (POST)");

  var exec = require("child_process").exec;
  exec("./deploy " + process.env.PM2_NAME, function (error, stdout, stderr) {
    console.log(stdout);
    if (error !== null) {
      console.log("Error during the execution of redeploy: " + stderr);
      return;
    }
  });

  res.status(200).send();
});

//GET
expressApp.get("/gitlab", function (req, res) {
  console.log("Received a gitlab hook event (GET)");

  var exec = require("child_process").exec;
  exec("./deploy " + process.env.PM2_NAME, function (error, stdout, stderr) {
    console.log(stdout);
    if (error !== null) {
      console.log("Error during the execution of redeploy: " + stderr);
      return;
    }

    //Retrieve last git status again
    var cmd = "git log -n 1";
    var exec = require("child_process").exec;
    exec(cmd, function (error, stdout, stderr) {
      if (error !== null) {
        var msg = "Error during the execution of git command: " + stderr;
        return res.send(msg);
      }
      res.status(200).send("Current git commit: " + stdout);
    });
  });

  //don't return immediately
  //res.status(200).send("deployed");
});

var server = expressApp.listen(process.env.GITLAB_PORT, function () {
  console.log("GitLab Hook on port " + server.address().port);
});
