// module.exports = {
//   apps : [{
//     name   : "test",
//     script : "node test.js",
//     instances : "max",
//     exec_mode : "cluster",
//     watch : false,
//     env : {
//         "PORT": 8081,
//         "PM2_NAME": "test"
//     },

//   }]
// }
module.exports = {
  apps : [{
    script    : "node test.js",
    instances : "max",
    exec_mode : "cluster",
    env : {
      "PORT": 8081,
      "PM2_NAME": "test"
    },
  }]
}