var express = require('express');
var cookieParser = require('cookie-parser')
var app = express();
var fs = require("fs");

app.use(cookieParser())
// var urlencodedParser = express.urlencoded({extended: false});
var multer = require('multer');

app.use(express.static('public'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(multer({dest: '/root/hoanglam'}).any());

app.get('/index', function(req, res){
    res.sendFile(__dirname + "/" + "index.html");
})

app.post('/file_upload', function(req, res){
   console.log(req.files);
   console.log(req.files[0].filename);
   console.log(req.files[0].path);
   console.log(req.files[0].mimetype);

   var file = __dirname + "/" + req.files[0].filename;
   fs.readFile( req.files[0].path, function (err, data) {
      fs.writeFile(file, data, function (err) {
         if( err ) {
            console.log( err );
            } else {
               response = {
                  message:'File uploaded successfully',
                  filename:req.files[0].filename
               };
            }
         
         console.log( response );
         res.end( JSON.stringify( response ) );
      });
   });
})


//API for verifying server is running, show latest git commit info
app.get("/", function (req, res) {
   var cmd = "git log -n 1";
   var exec = require("child_process").exec;
   exec(cmd, function (error, stdout, stderr) {
     if (error !== null) {
       var msg = "Error during the execution of git command: " + stderr;
       return res.send(msg);
     }
     res.status(200).send("Current git commit: " + stdout);
   });
 });
 
 //------------------------------------------------------------------------------
 // HTTP Server
 
 //Hook for gitlab auto deploy
 app.post("/gitlab", function (req, res) {
   console.log("Received a gitlab hook event (POST)");
 
   var exec = require("child_process").exec;
   exec("./deploy.sh " + process.env.PM2_NAME, function (error, stdout, stderr) {
     console.log(stdout);
     if (error !== null) {
       console.log("Error during the execution of redeploy: " + stderr);
       return;
     }
   });
 
   res.status(200).send();
 });
 
 //GET
 app.get("/gitlab", function (req, res) {
   console.log("Received a gitlab hook event (GET)");
 
   var exec = require("child_process").exec;
   exec("./deploy.sh " + process.env.PM2_NAME, function (error, stdout, stderr) {
      console.log(process.env.PM2_NAME);
     console.log(stdout);
     if (error !== null) {
       console.log("Error during the execution of redeploy: " + stderr);
       return;
     }
 
   });
 
   //don't return immediately
   //res.status(200).send("deployed");
 });

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})